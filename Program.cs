using System;

namespace ConsoleApp1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var Array = new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }; var random = new Random();
            foreach (var search in Array)
            {
                var index = Search(Array, search);
                Console.WriteLine($"{index} - {Array[index]}");
            }
        }
        static int Search(int[] Array, int search)
        {
            var minIndex = 0;
            var maxIndex = Array.Length - 1;

            while (true)
            {
                if (search == 1)
                {
                    return 0;
                }
                double del = 2;
                int middleIndex = Convert.ToInt32(Math.Ceiling((maxIndex + minIndex) / del));
                var middle = Array[middleIndex];
                if (middle < search)
                {
                    minIndex = middleIndex;
                }
                else if (middle > search)
                {
                    maxIndex = middleIndex;
                }
                else
                {
                    return middleIndex;
                    break;
                }
            }
        }
    }
}
